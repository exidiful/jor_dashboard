// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:test_app/card_services/create_afs_account.dart';
import 'package:test_app/card_services/create_jor_account.dart';
import 'package:test_app/card_services/create_virtual_card.dart';
import 'package:test_app/card_services/update_afs_account.dart';
import 'package:test_app/customer_services/details.dart';
import 'package:test_app/others/get_jumio_ref.dart';
import 'package:test_app/others/identification.dart';
import 'customer_services/archival.dart';
import 'customer_services/recovery.dart';
import 'others/manual_onboarding.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> customerServices = [
    CustomerRecoveryService(),
    CustomerArchivalService(),
    CustomerDetailsService(),
  ];
  List<String> customerServicesN = [
    "Customer Recovery",
    "Customer Archival",
    "Customer Details",
  ];
  List<Widget> cardServices = [
    CreateJORAccount(),
    UpdateAFSAccount(),
    CreateAFSAccount(),
    CreateVirtualCard(),
  ];
  List<String> cardServicesN = [
    "Create JOR Account",
    "Update AFS Account",
    "Create AFS Account",
    "Create Virtual Card",
  ];
  List<Widget> otherServices = [
    CreateIdentificationCase(),
    ManualOnboarding(),
    GetJumio()
  ];
  List<String> otherServicesN = [
    "Create Identification Case",
    "Manual Onboarding",
    "Get Jumio"
  ];

  int selectedIndex = 0;

  changeIndex(int num) {
    setState(() {
      selectedIndex = num;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: Row(
        children: [
          Container(
            width: 250,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
              ),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                FlutterLogo(
                  size: 200,
                ),
                Divider(),
                SizedBox(
                  height: 50,
                ),
                ListTile(
                  onTap: () => changeIndex(0),
                  leading: Icon(
                    Icons.person,
                    color: Colors.green,
                    size: 40,
                  ),
                  title: Text(
                    "Customers",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ListTile(
                  onTap: () => changeIndex(1),
                  leading: Icon(
                    Icons.card_membership,
                    color: Colors.green,
                    size: 40,
                  ),
                  title: Text(
                    "Accounts and cards",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ListTile(
                  onTap: () => changeIndex(2),
                  leading: Icon(
                    Icons.report,
                    color: Colors.green,
                    size: 40,
                  ),
                  title: Text(
                    "Others",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
          SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: selectedIndex == 0
                      ? [
                          customerServices[0],
                          customerServices[1],
                        ]
                      : selectedIndex == 1
                          ? [
                              cardServices[0],
                              cardServices[1],
                            ]
                          : [
                              otherServices[0],
                              otherServices[1],
                            ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: selectedIndex == 0
                      ? [
                          customerServices[2],
                        ]
                      : selectedIndex == 1
                          ? [
                              cardServices[2],
                              cardServices[3],
                            ]
                          : [
                              otherServices[2],
                            ],
                ),
              ],
            ),
          ),
        ],
      ),
      // bottomNavigationBar: SizedBox(
      //   height: 50,
      //   child: Row(
      //     children: customerServicesN
      //         .map((s) => Container(
      //             padding: EdgeInsets.all(10),
      //             child: Text(
      //               s,
      //               style: TextStyle(color: Colors.red),
      //             )))
      //         .toList(),
      //   ),
      // ),
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
